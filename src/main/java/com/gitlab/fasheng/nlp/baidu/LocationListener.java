/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 */

package com.gitlab.fasheng.nlp.baidu;

import android.util.Log;
import android.location.Location;
import org.microg.nlp.api.LocationHelper;
import com.baidu.location.BDAbstractLocationListener;
import com.baidu.location.BDLocation;

public class LocationListener extends BDAbstractLocationListener {

    private static final String TAG = "BaiduNlpLocationBackend";
    private static final String PROVIDER = "baidu";
    private static boolean indoorMode = false;

    @Override
    public void onReceiveLocation(BDLocation location) {
        if (location == null) {
            return;
        }

        if (location.getFloor() != null) {
            // 开启室内定位模式（重复调用也没问题），开启后，定位SDK会融合各种定位信息（GPS,WI-FI，蓝牙，传感器等）连续平滑的输出定位结果；
            if (!indoorMode) {
                indoorMode = true;
                Log.d(TAG, "onReceiveLocation: start indoor mode for " + location.getBuildingName());
                LocationBackend.instance.locationClient.startIndoorMode();
            }
        } else {
            // 关闭室内定位模式
            if (indoorMode) {
                indoorMode = false;
                Log.d(TAG, "onReceiveLocation: stop indoor mode");
                LocationBackend.instance.locationClient.stopIndoorMode();
            }
        }

        int locType = location.getLocType();
        if (locType == BDLocation.TypeOffLineLocation) {
            // 离线定位成功
            Log.d(TAG, "onReceiveLocation: offline location success");
        } else if (locType == BDLocation.TypeOffLineLocationFail) {
            // 离线定位失败
            Log.d(TAG, "onReceiveLocation: offline location faile");
        }
        double latitude = location.getLatitude();   // 获取纬度信息
        double longitude = location.getLongitude(); // 获取经度信息
        float accuracy = location.getRadius();      // 获取定位精度，默认值为0.0f

        // convert gcj02 to wgs84
        if (location.getCoorType().equals("gcj02")) {
            double []fixedLocation = Utils.gcj02towgs84(longitude, latitude);
            longitude = fixedLocation[0];
            latitude = fixedLocation[1];
        }

        Location response = LocationHelper.create(PROVIDER, latitude, longitude, accuracy);
        if (LocationBackend.instance != null) {
            Log.d(TAG, "response: locType=" + locType + " coorType=wgs84(from"
                  + location.getCoorType() + " " + location.getLatitude() + "," + location.getLongitude()
                  + ") networkLocationType=" + location.getNetworkLocationType() + " " + response);
            LocationBackend.instance.report(response);
        }
    }
}
